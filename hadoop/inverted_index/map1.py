#!/usr/bin/env python3
"""Map 1."""

import sys
import csv
import string
import collections
import re


stop_words_set = set()
with open("stopwords.txt", "r") as stop_words:
    for line in stop_words:
        line = line.split("\n")[0]
        stop_words_set.add(line)

csv.field_size_limit(sys.maxsize)
for row_of_csv_file in csv.reader(sys.stdin):
    doc_id = row_of_csv_file[0]
    document_title = row_of_csv_file[1]
    document_body = row_of_csv_file[2]

    # loop through the  and filter/parse each word from the document title and
    # body of any special chars, lowercase, any stop words
    document_title = document_title.split()
    for title_word in document_title:
        title_word = re.sub(r'[^a-zA-Z0-9]+', '', title_word)
        title_word = title_word.lower()
        if title_word in stop_words_set or not title_word:
            continue
        print(title_word + "_" + doc_id + "\t" + "1")

    document_body = document_body.split()
    for body_word in document_body:
        body_word = re.sub(r'[^a-zA-Z0-9]+', '', body_word)
        body_word = body_word.lower()
        if body_word in stop_words_set or not body_word:
            continue
        print(body_word + "_" + doc_id + "\t" + "1")

#!/usr/bin/env python3
"""Reduce2."""
import sys

for line in sys.stdin:
    print(line.strip('\n'))

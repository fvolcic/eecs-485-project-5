#!/usr/bin/env python3
"""Reduce2."""

import sys
from math import log10

num_docs = -1
with open("total_document_count.txt", "r") as tdc:
    line = tdc.readline()
    num_docs = int(line.strip('\n'))


current_key = ""
key_count = 0
doc_ids = []
for line in sys.stdin:
    line = line.strip('\n').split("\t")
    if not current_key:
        current_key = line[0]

    if current_key == line[0]:
        key_count += 1
        doc_ids.append(str(line[1]) + '\t' + str(line[2]))
    else:
        for doc_id in doc_ids:
            idf = log10(num_docs / key_count)
            print(doc_id + '\t' + str(current_key) + '\t' + str(idf))
            # print(str(current_key)+'\t'+doc_id+'\t'+str(idf))
        doc_ids = []
        doc_ids.append(str(line[1]) + '\t' + str(line[2]))
        current_key = line[0]
        key_count = 1

for doc_id in doc_ids:
    idf = log10(num_docs / key_count)
    print(doc_id + '\t' + str(current_key) + '\t' + str(idf))
    # print(str(current_key)+'\t'+doc_id+'\t'+str(idf))

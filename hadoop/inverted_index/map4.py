#!/usr/bin/env python3
"""Reduce2."""
import sys

for line in sys.stdin:
    # word , freq, docId, idf, normalization
    print(line.strip('\n'))

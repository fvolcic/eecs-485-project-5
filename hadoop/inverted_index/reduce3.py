#!/usr/bin/env python3
"""Reduce2.

The goal of reduce2 is to compute the norm of all documents.
Here, the key is the document ID. At the end of this stage, the key
should again be the word, and not the document ID.

"""

import sys

# <doc_id> <word_count> <word> <idf>


####
# THIS IS THE BUGGY PROGRAM
####

# input format: <doc_id> <word_count> <word> <idf>
# output format: <word> <word count> <doc_id> <idf> <norm>

current_key = ""
current_idf = ""

running_norm = 0

word_info = []


for line in sys.stdin:
    line = line.strip('\n').split('\t')

    if not current_key:
        current_key = line[0]

    if line[0] == current_key:
        # add multiply (word_count)^2 * (word-idf)^2
        running_norm += (float(line[1])**2) * (float(line[3])**2)

        # save word, word count, and the word idf for writing later
        word_info.append(
            line[2] +
            '\t' +
            line[1] +
            '\t' +
            line[0] +
            '\t' +
            line[3])
    else:
        for info in word_info:
            print(info + '\t' + str(running_norm))

        # reset some of the needed parameters
        word_info = []
        word_info.append(
            line[2] +
            '\t' +
            line[1] +
            '\t' +
            line[0] +
            '\t' +
            line[3])
        running_norm = (float(line[1])**2) * (float(line[3])**2)
        current_key = line[0]

for info in word_info:
    print(info + '\t' + str(running_norm))

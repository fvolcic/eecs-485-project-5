#!/usr/bin/env python3
"""Reduce 1."""

import sys


firstRun = True
currKey = ""
line = sys.stdin.readline()
while True:
    numEntries = 0
    if line and firstRun:
        firstRun = False
        currKey = line.split()[0]
    while line and line.split()[0] == currKey:
        numEntries += 1
        line = sys.stdin.readline()
    currKey = currKey.split('_')
    print(currKey[0] + "\t" + currKey[1] + "\t" + str(numEntries))
    if not line:
        break
    else:
        currKey = line.split()[0]

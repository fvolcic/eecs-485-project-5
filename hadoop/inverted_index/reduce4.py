#!/usr/bin/env python3
"""Reduce2."""

import sys

current_key = ""

for line in sys.stdin:
    line = line.strip('\n')
    line = line.split('\t')
    # <term> <idf> <doc_id_x> \
    # <occurrences in doc_id_x> <doc_id_x normalization factor before sqrt>\
    # <doc_id_y> <occurrences in doc_id_y>
    # <doc_id_y normalization factor before sqrt> ...
    if not current_key:
        current_key = line[0]
        print(current_key + ' ' + line[3], end='')

    if line[0] == current_key:
        print(' ' + line[2] + ' ' + line[1] + ' ' + line[4], end='')

    else:
        print()
        current_key = line[0]
        print(current_key + ' ' + line[3], end='')
        print(' ' + line[2] + ' ' + line[1] + ' ' + line[4], end='')

print()
